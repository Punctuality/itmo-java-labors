package MVCLabor1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class View {

    static Scanner scanner = new Scanner(System.in);

    public static String dirs_term_message() {
        System.out.println("Will we work with terminal or with files?");
        System.out.print("Enter [1|2]: ");
        String out = scanner.nextLine();
        return out;
    }
    public static String input_message(int i_max, int j_max) {
        System.out.print(String.format("Input dimensions [a b] (a<=%d) (b<=%d):", i_max, j_max));
        String out = scanner.nextLine();
        return out;
    }

    public static String input_message_file(int i_max, int j_max){
        System.out.println(String.format("File should be filled with: Input dimensions [a b] (a<=%d) (b<=%d)", i_max, j_max));
        System.out.print("Input file's path: ");
        String path = scanner.nextLine();
        File file = new File(path);
        String out = "";
        try {
            Scanner file_scan = new Scanner(file);
            out = file_scan.nextLine();
            file_scan.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            out = "error";
        }
        return out;
    }

    public static String output_message_file(){
        System.out.print("Enter file path to save answer: ");
        String path = scanner.nextLine();
        return path;
    }

    public static void matrix_show(String[] parsed_lines){
        for(String line : parsed_lines){
            System.out.println(line);
        }
    }

    public static void matrix_show_file(String[] parsed_lines, String path){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(path);
            for(String line : parsed_lines){
                writer.println(line);
            }
            writer.flush();
            writer.close();
            System.out.println("Writing finished.");
        } catch (FileNotFoundException e) {
            System.out.println("File not found! Writing not finished!");
        }

    }



    public static void stop_programm(){
//        scanner.close();
        System.out.println("Execution end.");
    }

}
