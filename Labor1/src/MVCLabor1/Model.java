package MVCLabor1;

import java.util.Random;

public class Model {

    static long[] h;
    static float[] x;

    final static long[] check_list = {5, 9, 11, 12, 13, 14, 16, 20};
    final static long check_value = 10;

    public static int[] init(){
        //Создать одномерный массив h типа long.
        //Заполнить его числами от 5 до 20 включительно в порядке возрастания.
        h = new long[(20 - 5) + 1];
        for (int i = 0; i < h.length; i++) {
            h[i] = 5 + (long) i;
        }
        //Создать одномерный массив x типа float.
        // Заполнить его 19-ю случайными числами в диапазоне от -4.0 до 3.0.
        x = new float[19];
        Random rnd = new Random();
        for (int i = 0; i < x.length; i++) {
            x[i] = rnd.nextFloat() * (float) 7.0 - (float) 4.0;
        }

        int[] out = {h.length, x.length};
        return out;
    }

    public static double[][] matrix_gen(int[] dims){
        //Тут я не решился комментить задание.
        double[][] v = new double[dims[0]][dims[1]];

        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[0].length; j++) {
                if(h[i] == check_value){
                    v[i][j] = Math.pow(Math.asin(Math.pow(Math.E, -1.0*Math.abs(x[j]))), 3.0);
                } else if (check_inclusion(h[i], check_list)){
                    v[i][j] = Math.log(Math.pow(Math.cos(Math.pow(((Math.sin(x[j]) - 0.25)/4.0), 2.0)),2.0));
                } else {
                    v[i][j] = 2.0 * Math.pow(Math.E, Math.sin(x[j])+1.0);
                }
            }
        }

        return v;
    }

    // Этот метод чекает наличие элемента в массиве.
    public static boolean check_inclusion(long elem, long[] arr) {
        boolean is_in = false;
        for (long i : arr) {
            if (i == elem) {
                is_in = true;
                break;
            }
        }
        return is_in;
    }

    public static int[] reader(String input, int i_max, int j_max){
        int[] out_dims = {0,0};
        try {
            String[] out = input.split(" ");
            for(int i = 0; i<out_dims.length; i++){
                out_dims[i] = Integer.parseInt(out[i]);
            }
            if (out_dims[0] > i_max || out_dims[1] > j_max || out.length > 2){
                out_dims[0] = 0;
                out_dims[1] = 0;
                throw new RuntimeException();
            }
        } catch (Exception e) {
            System.out.println("Incorect input!");
            Control.go = false;
        }
        return out_dims;
    }

    public static int dims_term(String input){
        int out_num = 0;
        try {
            out_num = Integer.parseInt(input);
            if (out_num != 1 && out_num != 2) {
                out_num = 0;
                throw new RuntimeException();
            }
        } catch (Exception e) {
            System.out.println("Incorect input!");
        }
        System.out.println();
        return out_num;
    }

    public static String[] matrix_shower(double[][] mat) {
        String[] out = new String[mat.length];
        for (int i = 0; i < mat.length; i++) {
            out[i] = line_parser(mat[i]);
        }
        return out;
    }

    private static String line_parser(double[] arr) {
        String res = "";
        for (double i : arr) {
            res += Math.round(i * 1e+3) / 1e+3 + " ";
        }
        return res;
    }
}
