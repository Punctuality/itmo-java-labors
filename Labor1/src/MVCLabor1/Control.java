package MVCLabor1;

public class Control {

    static boolean terminals;

    public static boolean go = true;

    public static void main(String[] args) {
        int[] inits = Model.init();
        String inp1 = View.dirs_term_message();
        int choose = Model.dims_term(inp1);
        if (choose == 1) {
            terminals = true;
        } else if (choose == 2) {
            terminals = false;
        } else {
            View.stop_programm();
            go = false;
        }
        if (go) {
            int[] matrix_dims = get_dims(terminals, inits);

            if (go) {
                double[][] matrix = Model.matrix_gen(matrix_dims);

                answrs_out(terminals, matrix);
            }

            View.stop_programm();
        }

    }

    private static void answrs_out(boolean terminals, double[][] matrix) {
        String[] out = Model.matrix_shower(matrix);
        if (terminals) {
            View.matrix_show(out);
        } else {
            String path = View.output_message_file();
            View.matrix_show_file(out, path);
        }
    }

    private static int[] get_dims(boolean terminals, int[] inits) {
        String inp2 = "";
        if (terminals) {
            inp2 = View.input_message(inits[0], inits[1]);
        } else {
            inp2 = View.input_message_file(inits[0], inits[1]);
        }
        return Model.reader(inp2, inits[0], inits[1]);
    }
}
